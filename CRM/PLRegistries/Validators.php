<?php

class CRM_PLRegistries_Validators {

  public $availableValidators = array(
    'regon' => 'REGON',
    'nip' => 'NIP',
    'pesel' => 'PESEL',
  );

  /**
   * Validate REGON
   *
   */
  function validate_regon($str) {
    $str = _format_REGON($str);
    if (strlen($str) == 9) {
      return preg_match('/^[0-9]{9}$/', $str) 
          && $this->_validate_regon9($str);
    }
    elseif (strlen($str) == 14) {
      return preg_match('/^[0-9]{14}$/', $str) && 
          $this->_validate_regon9($str) && 
          $this->_validate_regon14($str);
    }
    else {
      return false;
    }
  }

  private function _validate_regon9($str) {
    $arrSteps = array(8, 9, 2, 3, 4, 5, 6, 7);
    $intSum = 0;
    for ($i = 0; $i < 8; $i++) {
      $intSum += $arrSteps[$i] * $str[$i];
    }
    $int = $intSum % 11;
    $intControlNr = ($int == 10) ? 0 : $int;
    if ($intControlNr == $str[8]) {
      return true;
    }
    return false;
  }

  private function _validate_regon14($str) {
    $arrSteps = array(2, 4, 8, 5, 0, 9, 7, 3, 6, 1, 2, 4, 8);
    $intSum = 0;
    $intSumReset = 0;
    for ($i = 0; $i < 13; $i++) {
      $intSum += $arrSteps[$i] * $str[$i];
      if ($i > 9) {
        $intSumReset+=$str[$i];
      }
    }
    $int = $intSum % 11;
    $intControlNr = ($int == 10) ? 0 : $int;
    if ($intControlNr == $str[13] || $intSumReset == 0) {
      return true;
    }
    return false;
  }

  /**
   * Validate NIP
   *
   */
  function validate_nip($str) {
    $nip = preg_replace("/[^a-zA-Z0-9]+/", "", $str);
    if (strlen($nip) != 10) {
      return false;
    }

    $steps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
    $checkSum = 0;

    for ($i = 0; $i < 9; $i++) {
      $checkSum += $steps[$i] * $nip[$i];
    }
    $checkSum = $checkSum % 11;
    return ( $checkSum == $nip[9] ) ? true : false;
  }

  /**
   * Validate PESEL
   *
   */
  function validate_pesel($str) {
    if (!preg_match('/^[0-9]{11}$/', $str)) { //check 11 digits
      return false;
    }
    $arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3); // weights
    $intSum = 0;
    for ($i = 0; $i < 10; $i++) {
      $intSum += $arrSteps[$i] * $str[$i]; //multiply each digit by weight and sum
    }
    $int = 10 - $intSum % 10; //calculate checksum
    $intControlNr = ($int == 10) ? 0 : $int;
    if ($intControlNr == $str[10]) { //check calculated checksum
      return true;
    }
    return false;
  }

}
