<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_PLRegistries_Form_Config extends CRM_Core_Form {

  function buildQuickForm() {

    // add form elements
    $params = array(
      'version' => 3,
      'sequential' => 1,
    );
    $result = civicrm_api('CustomField', 'get', $params);

    foreach ($result['values'] as $custom_field) {
      $this->add(
          'select', // field type
          'validator_' . $custom_field['id'], // field name
          'Validator', // field label
          $this->getValidators(), // list of options
          false // is required
      );
    }

    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => ts('Submit'),
        'isDefault' => TRUE,
      ),
    ));

    // export form elements
    $this->assign('validatorOptions', $this->getValidators());

    $params = array('version' => 3, 'sequential' => 1, 'name' => 'plregistries');
    $result = civicrm_api('Setting', 'getvalue', $params);

    parent::buildQuickForm();
  }

  function postProcess() {
    $values = $this->exportValues();
    $options = $this->getValidators();
    $validation_settings = array();
    foreach ($values as $key => $val) {
      if (!empty($val) && substr($key, 0, 10) == 'validator_') {
        $validation_settings[substr($key, 10)] = $val;
      }
    }
    $config = CRM_Core_Config::singleton();
    $params = array(
      'version' => 3,
      'sequential' => 1,
      'domain_id' => $config->domainID(),
      'plregistries' => $validation_settings,
    );
    civicrm_api('setting', 'create', $params);
    CRM_Core_Session::setStatus(ts('Validator settings has been saved'));
    parent::postProcess();
  }

  function getValidators() {
    require_once 'CRM/PLRegistries/Validators.php';
    $validators = new CRM_PLRegistries_Validators();
    $options = array(
      '' => ts('- select -')
    );
    return array_merge($options, $validators->availableValidators);
  }

  /**
   *
   * @see CRM_Core_Form::setDefaultValues()
   *
   */
  function setDefaultValues() {
    $params = array('version' => 3, 'sequential' => 1, 'name' => 'plregistries');
    $result = civicrm_api('Setting', 'getvalue', $params);
    $defaults = array();
    foreach ($result as $key => $value) {
      $defaults['validator_' . $key] = $value;
    }
    return $defaults;
  }

}
