# CiviCRM Extension: pl.infozmiana.registries #
## Polish national registries numbers validation ##

### README (EN) ###

This module extension provides Polish national registries numbers validators for custom fields. Registries included are: REGON, NIP and PESEL.
 
However weird this might sound, this extension also contains Drupal module accompanying it (in drupal/ directory), making the same validators available to CiviCRM enabled webforms. The module won't work without extension enabled and working. To install it, copy the pivext_webform/ directory to your usual Drupal modules directory and turn it on.

### README (PL) ###

To rozszerzenie do CiviCRMa pozwala na włączenie dla pól dodatkowych walidacji dla numerów polskich rejestrów państwowych. Obsługiwane rejestry to REGON, NIP oraz PESEL.    

Jakkolwiek dziwnie by to nie brzmiało, to rozszerzenie zawiera również moduł drupalowy oferujący te same walidatory dla webformów z włączoną integracją z CiviCRM. Moduł ten nie będzie działać bez zainstalowanego i działającego rozszerzenia. Aby go zainstalować, skopiuj katalog pivext_webform/ do zwyczajowego drupalowego miejsca na moduły i włącz go.