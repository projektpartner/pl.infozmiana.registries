<?php

require_once 'registries.civix.php';

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function registries_civicrm_config(&$config) {
  _registries_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function registries_civicrm_xmlMenu(&$files) {
  _registries_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function registries_civicrm_install() {
  return _registries_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function registries_civicrm_uninstall() {
  return _registries_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function registries_civicrm_enable() {
  return _registries_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function registries_civicrm_disable() {
  return _registries_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function registries_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _registries_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function registries_civicrm_managed(&$entities) {
  return _registries_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function registries_civicrm_caseTypes(&$caseTypes) {
  _registries_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function registries_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _registries_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implementation of hook_civicrm_validateForm
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function registries_civicrm_validateForm($formName, &$fields, &$files, &$form, &$errors) {
  // FIXME Refactor this function a little bit

  $params = array('version' => 3, 'sequential' => 1, 'name' => 'plregistries');
  $result = civicrm_api('Setting', 'getvalue', $params);

  require_once 'CRM/PLRegistries/Validators.php';
  $facility = new CRM_PLRegistries_Validators();

  foreach ($result as $custom_field_id => $field_type) {
    // FIXME Needs to be tested for actual multivalue fields
    // FIXME Won't work for address fields, since $fields array is not searched recursively
    $field_name = "custom_" . $custom_field_id;
    $search_key = preg_grep('/^custom_' . $custom_field_id . '/', array_keys($fields));

    if (!empty($search_key)) {
      foreach ($search_key as $field_name) {

        $field_value = CRM_Utils_Array::value($field_name, $fields);
        if (array_key_exists($field_type, $facility->availableValidators)) {
          $validator_name = 'validate_' . $field_type;
          if (!$facility->$validator_name($field_value)) {
            $errors[$field_name] = "This isn't proper number of type: " .
                strtoupper($field_type);
          }
        }
      }
    }
  }
  return;
}
