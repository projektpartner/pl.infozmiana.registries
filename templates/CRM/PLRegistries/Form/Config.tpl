{* HEADER *}

<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="top"}
</div>


{crmAPI var='result' entity='CustomGroup' action='get' sequential=1}
{foreach from=$result.values item=CustomGroup}
  <h3>{ts}Custom Field Set{/ts}: <strong>{$CustomGroup.title}</strong></h3>
  <table class="selector">
    <tr class="columnheader">
      <th width="4%">Id</th>
      <th>Label</th>
      <th width="35%">Validator</th>
    </tr>
{crmAPI var='result' entity='CustomField' action='get' sequential=1 custom_group_id=$CustomGroup.id}
{foreach from=$result.values item=CustomField}
<tr class="crm-job {cycle values="odd-row,even-row"}">  
  <td>{$CustomField.id}</td> 
  <td>{$CustomField.label}</td>
  <td>
  <div class="crm-section">
    <div class="content">
      <div>
        {assign var="validatorId" value="validator_`$CustomField.id`"}
        <span>{$form.$validatorId.html}</span>
      </div>    
    </div>
  </div>
  </td>
</tr>
{/foreach}  

  </table>

{/foreach}





{* FOOTER *}
<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>
